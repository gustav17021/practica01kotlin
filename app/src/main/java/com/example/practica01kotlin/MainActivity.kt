package com.example.practica01kotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var btnPulsar: Button
    private lateinit var btnBorrar: Button
    private lateinit var btnTerminar: Button
    private lateinit var txtNombre: EditText
    private lateinit var lblSaludar: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //relacionar los objetos
        btnPulsar = findViewById(R.id.btnSaludar)
        btnBorrar = findViewById(R.id.btnLimpiar)
        btnTerminar = findViewById(R.id.btnCerrar)
        txtNombre = findViewById(R.id.txtNombre)
        lblSaludar = findViewById(R.id.lblSaludo)

        //Codificar el evento clic del boton
        btnPulsar.setOnClickListener(View.OnClickListener {
            //Validar si nombre está vacío
            if (txtNombre.getText().toString()=="") {
                //Toast - context es para saber sobre dónde está trabajando, en este caso es MainActivity
                Toast.makeText(
                    this@MainActivity, "Faltó capturar información",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val str = "Hola " + txtNombre.getText().toString() + " ¿Cómo estás?"
                lblSaludar.setText(str)
            }
        })

        //Boton Limpiar
        btnBorrar.setOnClickListener(View.OnClickListener { //Validar si nombre está vacío
            if (txtNombre.getText().toString()=="") {
                Toast.makeText(
                    this@MainActivity, "No hay algo que borrar",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                txtNombre.setText("")
                lblSaludar.setText(":: ::")
            }
        })

        //Boton Cerrar
        btnTerminar.setOnClickListener(View.OnClickListener { finish() })
    }
}